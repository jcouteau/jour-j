# Consignes

Nous avons décidé de faire cette section pour lister toutes les consignes/contraintes du mariage

## Animaux interdits

L'Escampette qui nous accueille pour la fête a beaucoup d'animaux en liberté ou semi-liberté (ânes, oies, canards,
lapins). Les chiens sont donc interdits (sauf les chiens guides, mais à notre connaissance il ne devrait pas y en avoir).

On voudrait éviter de voir arriver un chien avec un lapin dans la gueule au beau milieu du repas, merci.

## Repas partagé

Nous avons choisi d'opter pour un repas participatif. Le principe est simple, venez avec un plat salé à partager. Là encore,
pas de contraintes, vous faites comme vous voulez et vous faites ce qui vous fait plaisir (votre plat préféré, une
salade, le plat de votre traiteur favori, un melon...) le principal pour nous est de passer un moment simple et
chaleureux. Nous nous chargerons du dessert et des boissons.

## Bibliothèque des mariés

Pas de liste de mariage. À la place comme nous sommes tous les deux des lecteurs insatiables, nous aimerions que vous
veniez avec un livre que vous avez envie de nous faire découvrir, un livre qui vous a touché, ému ou fait rire, voyager...
Cela peut être un roman, une BD, un livre d'art, ce que vous voulez.