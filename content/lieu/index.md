---
title: "Où c'est ?"

---

# La mairie

Pour la mairie, nous serons à la Mairie centrale de Nantes.

![Mairie de Nantes](mairie.jpg)

Pour les accès transports en commun, c'est facile : tramway Ligne 1, arrêt "Bouffay" et vous terminez à pied ou alors
l'arrêt "Hôtel de Ville" desservis par les bus C1, C6, 11 et 12.

Pour les parkings, ça peut être très compliqué dans le quartier, le plus simple c'est de se garer au parking
Decré-Bouffay. Par contre attention, c'est serré et limité à 1m80 en hauteur ! Pour les plus gros gabarits, il faudra
prendre le parking Feydeau (arrêt de tramway "Bouffay", limite à 2m15) et remonter à pied, sinon le parking Baco-Lu
(enclos, pas de limite de taille et idem, finir à pied).

# La fête

La fête aura lieu au milieu des champs, le long du Canal de Nantes à Brest à Saffré. Une fois arrivé, tout est sur place
(cérémonie laïque, vin d'honneur, repas, soirée dansante).

![La tente de restauration de l'Escampette](escampette.jpeg)

## Venir en voiture

L'adresse :

```
L'escampette
Le camp
44390 Saffré
```

Coordonnées GPS :

```
47.4608, -1.6254
```

![Carte avec Nantes et L'Escampette](escampette.png)

Itinéraire Mairie Escampette en voiture : https://www.openstreetmap.org/directions?engine=fossgis_osrm_car&route=47.2182%2C-1.5539%3B47.4608%2C-1.6254#map=13/47.2539/-1.5249

## Venir en vélo/transports doux

Malheureusement, pas de transport en commun. Les motivés peuvent venir en vélo, c'est fléché quasi depuis la mairie (Vélodyssée - 40 km quand même)
(pour les footeux, l'itinéraire passe devant La Jonelière). Au départ de la gare de Nantes, ça change sur les 2 premiers km, après c'est pareil

<iframe src='https://connect.garmin.com/modern/course/embed/108989454' width='465' height='548' frameborder='0'></iframe>

# Et ceux qui ne sont pas à la cérémonie/repas, ils font quoi ?

![L'Étang de Bout-de-Bois](2019-bout-de-bois-drone-peupladestv-6-.jpg)

À 15 minutes à pied de L'Escampette, il y a l'étang de Bout-de-Bois avec une plage (mais baignade interdite à cause
de la qualité de l'eau), une guinguette ([La guinguette des Janettes](https://www.facebook.com/lesJanettes/)) qui fait à manger, location
de kayaks/canoës/paddles ([Canal Kayak](https://canalkayak.fr)), jeu de piste
([Baludik](https://baludik.fr/balades/fiche_balade_parcours-b793-percez-les-secrets-du-canal)).

![Paddle et Kayak](paddle.jpg)

PS : À noter que ces activités sont aussi disponibles le lendemain ou après pour ceux qui veulent prolonger dans les alentours ;)