# Programme

## Samedi

### 10h40 Mairie (à Nantes, pour les concernés)

Le rendez-vous est à 10h15 devant la mairie où un huissier nous ammènera dans la salle des mariages.

### 13h Pique nique champêtre (à Saffré, pour les concernés)

### 15h Cérémonie laïque (à Saffré)

### 17h Vin d'honneur (à Saffré)

### 20h Repas partagé (à Saffré)

Se référer aux [consignes]({{<ref "consignes/index.md#repas-partagé">}})

### 22h Soirée dansante déguisée (à Saffré)

Se référer aux [consignes]({{<ref "theme/index.md#la-soirée">}})

## Dimanche

### À partir de 10h Brunch (à Saffré)

## Tout le week-end

La [bibliothèque des mariés]({{<ref "consignes/index.md#bibliothèque-des-mariés">}}) attendra d'être remplie.