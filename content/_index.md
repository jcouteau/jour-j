---
title: "Mariage J & J - 12-13 août 2023"
featured_image: '/images/titre.png'
---
Bienvenue sur le site de notre mariage. Vous trouverez toutes les informations utiles pour le jour J. On a hâte de
vous recevoir et de faire la fête avec vous.

Nous aimons tous les deux les grands espaces et la nature, nous aimons aussi la simplicité et ne pas se prendre la tête.
On vous a concocté un mariage qui nous ressemble : simple et pas classe, foutraque et hétéroclite, chaleureux et accueillant.
Nous vous accueillerons à la campagne dans un écolieu bucolique et insolite dans un cadre naturel et préservé au
bord du canal de Nantes à Brest. Nous n'avons pas choisi de thème pour laisser la liberté à chacun de venir
comme il le souhaite.