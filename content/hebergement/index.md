---
title: "L'hébergement"
---

Nous listons ici tous les hébergements possibles pour la nuit du samedi au dimanche. Pour les tentes nomades, merci de
nous dire avant le 30/11/2022 si vous êtes intéressés. Nous nous occuperons de répartir les personnes dans les tentes en
fonction des choix (les familles avec jeunes enfants seront prioritaires sous ces tentes). Pour ceux qui souhaitent plus
de confort, c'est en bas de la page (et non exhaustif), et on vous laisse gérer vos réservations.

# Le "camping sauvage"

![Le terrain de camping](camping.png)

Vous pouvez planter votre tente sur un espace camping sauvage près d'une pâture avec des ânes sur une surface plane et
tondue de 4000 m2 (avec des parties ombragées)

Des toilettes sèches, un espace avec lavabo et eau potable, lumière, prises électriques et une douche extérieure sont accessibles.

![Les ânes et le terrain de camping](camping-2.png)

Et en plus il y a une zone pour les camping-cars, vans, tentes de toit, camion aménagé, caravane ou autre logement sur roue.

# Tentes nomades (41 places)

Sept grandes tentes nomades collectives de 20m2 sont disponibles sous les chênes centenaires (dont une PMR). Elles sont
équipées de matelas avec sommier (quelques matelas au sol) et de lits de camp (pour les enfants).

![L'intérieur d'une tente nomade](tenteNomade.png)

- 11 lits doubles
- 1 lit 120
- 8 lits simples
- 10 lits de camp

A prévoir :
- Duvet ou couette
- Lampe de poche
- Boules Quies

On prévoira une cagnotte pour le réglement, compter 10 € par personne pour un lit de camp (avec plaid et coussin) et
20€ par personne pour un matelas (avec drap housse, oreiller/taie et couverture)

Ce type d'hébergement est plutôt réservé aux parents avec jeunes enfants (qui pourront se coucher plus tôt)

![Une tente nomade](tenteNomade2.png)

/!\ : Il est interdit de manger ou boire, de rentrer un animal dans les chambres et tentes !

Il est possible de remplacer un lit de camp dans les tentes par votre "lit parapluie" pour
un bébé.

/!\ : Dites nous si vous souhaitez une place avant le 30/11/2022 !

# Gites

## Gîte Le Plessis Pas Brunet

À 11 minutes en voiture. 5 chambres de 2 à 5 personnes : https://www.rando-accueil.com/hebergement/Hebergement-Rando-Accueil-662.htm

## Chambres d'hôtes "Écluse de la Tindière"

À 11 minutes en voiture. 2 chambres de 2 et 4 personnes : 06 70 04 45 09 ; https://www.etapecanalgiteeclusedelatindiere.com

## Chambre d'hôte "Chez Béa et Thierry"

À 7 minutes en voiture. 2 chambres disponibles à partir de 60€ : https://www.chambresdhotes.org/Detailed/211385.html

## Chambre d'hôte Escal Canal 44

À 11 minutes en voiture. 2 chambres disponibles à partir de 51 € : http://chambrecanalblainescale.fr

## Chambres d'hôte de Vioreau

À 22 minutes en voiture. 3 chambres au bord du lac (pour ceux qui veulent combiner avec les vacances) : https://www.vioreau.com

## Hôtel L'abreuvoir ***

À 7 minutes en voiture. 24 chambres : https://hotel-abreuvoir.fr

## Camping Le Port Mulon

À 14 minutes en voiture. Mobil home ou chalet 4/5 personnes : 02 40 72 23 57 - https://www.camping-portmulon.com