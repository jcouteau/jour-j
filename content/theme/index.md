# Le thème

## La journée

Pour la journée, nous n'imposons pas de thème. Cependant, le lieu est champêtre, avec une atmosphère
particulière (entre la guinguette, la ferme et la brocante), cela peut orienter votre recherche de tenue. Pour les
femmes, nous recommandons d'éviter tout ce qui se rapproche de talons aiguilles (au risque de s'enfoncer de quelques
centimètres dans le sol).

Photo Escampette

## La soirée

Pour la soirée, par contre, c'est déguisement O-BLI-GA-TOI-RE. Chevalier ou princesse, personnage de film ou de fiction,
objet du quotidien, animal, fabrication maison ou import du magasin, pas de limite à votre imagination (attention, il
faut quand même pouvoir danser) ! Surprenez-nous !

![Idées de déguisements](deguisements.png)
